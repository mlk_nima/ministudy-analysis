# Loading Libraries ------
library(dplyr)
library(ggplot2)
library(grid)
library(gridExtra)
library(Matrix)
library(plotrix)
library(plot3D)
library(rgl)
require(graphics)
library(rjson)
library(rgeolocate)
library(Rcampdf)
library(Matrix)

#read data and convert -----------------
rawRatings = read.csv("raw_ratings.csv", sep = ";")
rawRatings = as.data.frame(rawRatings)

#Creating the rating matrix --------------

#initialize vector of accumulated ratings.
urm = data.frame(song = rep(1:200, each = 1), score = 0)

#find sum and average of ratings for each song
for(i in 1:200)
{
avgScore = 0
filtered = filter(rawRatings,song==i)
ratings = select(filtered,rating)
if (nrow(ratings)>0){
avgScore = sum(ratings) / count(ratings)#is the average rating of each song.
}
urm[i,2] = avgScore
}

write.csv(urm, file = "AllSongsGeneralPopularity.csv",row.names=FALSE)

#sort the ratings based on popularity
urmSorted = arrange(urm,desc(score))
write.csv(urmSorted, file = "AllSongsSortedGeneralPopularity.csv",row.names=FALSE)

#remove the songs under a certain popularity threshold, plotting the thing and saving ---------------
threshold = 0
urmThreshouldApplied = filter(urm,score>threshold)
urmThreshouldApplied$song <- factor(urmThreshouldApplied$song, levels = urmThreshouldApplied$song[order(urmThreshouldApplied$score,decreasing = FALSE)])
write.csv(urmThreshouldApplied, file = "PopularSongsGeneralPopularity.csv",row.names=FALSE)
ggplot(urmThreshouldApplied,aes(x=song,y=score)) + geom_bar(stat="identity", aes(fill = song),width=0.5)+ labs(title = "General popularity of songs", y="Average score (stars)", x="Song") + theme(legend.position="none") + coord_flip()

# for the above reason we need to map the distribution -----------------------
# to a probability distribution that is falling quickly at the beginning and is then flat at the tail
for (i in 1:200){
  urmSorted[i,3] = 1 / i
  urmSorted[i,4] = count(filter(rawRatings,song==urmSorted[i,1]))
}
colnames(urmSorted)[3] = "probability"
colnames(urmSorted)[4] = "numberOfRatings"

#normalizing (0,1)
for (i in 1:200){
  urmSorted[i,5] = urmSorted[i,3] / sum(urmSorted$probability)
}
colnames(urmSorted)[5] = "normalizedProbability"

sum(urmSorted$normalizedProbability) #equals to 1.0 #No Surprise!

#create a large bin for PHP to select a random number from. Songs are picked following our newly generated custom distribution.
bin <- sample(x = urmSorted$song, 1000, replace = T, prob = urmSorted$normalizedProbability)
bin = as.data.frame(bin)
colnames(bin)[1] = "song"
ggplot(bin,aes(bin)) + geom_bar(fill = "dodgerblue") + labs(title = "Frequency of songs in the bin for PHP", y="Times appeared in the bin of 1000 song IDs", x="Song ID") + coord_flip() +scale_x_continuous(breaks=c(1:200)) 
#Isn't it too steep? A handful of songs are extremely likely to pop up in the comparisons.

write.table(bin, file="binForPHP.csv", sep=",", col.names=FALSE, row.names=FALSE)